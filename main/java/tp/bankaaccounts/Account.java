package tp.bankaaccounts;

import java.io.Serializable;
import java.util.Date;
import java.util.Observable;

/**
 * Aceasta clasa reprezinta entitatea unui cont. Aceasta contine proprietarul
 * contului. In afara de acesta, contine numarul contului bancar,
 * identificatorul unic pentru indetificarea contului si suma de bani depusa in
 * acel cont
 *
 * Extinde observable pentru a notifica observerii cand se produce o schibare pe cont. Observerul este clientul la care apartine acest cont.
 *
 * @author olarp
 */
public class Account extends Observable implements Serializable {

    protected int number;
    protected Client client;
    protected double amount;
    protected Date date_deposited;

    /**
     * Constructor pentru implementarea subclaselor
     */
    public Account() {
    }

    /**
     * Constructor care stabileste parametrii in campurile corespunzatoare
     *
     * @param number Numar de cont
     * @param client Proprietarul contului
     * @param solde  Suma de bani in cont
     * @param mydate Data depunerii/retragerii
     */
    public Account(int number, Client client, double solde, Date mydate) {
        this.number = number;
        this.client = client;
        this.amount = solde;
        this.date_deposited = mydate;
    }

    /**
     * Metoda de intretinere care returneaza numarul contului
     *
     * @return
     */
    public int getAccount() {
        return number;
    }

    /**
     * Metoda care returneaza numarul de cont
     *
     * @return
     */
    public int getNumber() {
        return this.number;
    }

    public Client getOwner() {
        return this.client;
    }

    public double getAmount() {
        return this.amount;
    }

    public Date getDate() {
        return this.date_deposited;
    }

    /**
     * Metoda de suprascriere care returneaza clientul si suma de bani din cont
     */
    @Override
    public String toString() {
        return client.toString() + amount;
    }

}
