package tp.bankaaccounts;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Un frame care detine componentele swing si permite utilizatorului sa
 * interactioneze cu sistemul
 *
 * @author olarp
 */
public class AdminFrame extends javax.swing.JFrame {
    private static final int SAVING = 0;
    private static final int SPENDING = 1;
    private TableModel accModel, clientsModel;
    private ArrayList<Account> myAccounts;
    private int currID = 0;
    private String currCNP = "";
    private JTable accTable;
    private javax.swing.JButton changeBtn;
    private JTable clientsTable;
    private javax.swing.JLabel cnpTxt;
    private javax.swing.JLabel currAccTxt;
    private javax.swing.JButton delAccBtn;
    private javax.swing.JButton deleteClient;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JButton logoutBtn;
    private javax.swing.JButton showAccsBtn;
    private javax.swing.JButton showclBtn;

    public AdminFrame() {
        initComponents();
        myAccounts = new ArrayList<Account>(1000);
        this.addListener(accTable, 0);
        this.addListener(clientsTable, 1);
    }

    /**
     *
     */
    @SuppressWarnings("unchecked")
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        logoutBtn = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        accTable = new JTable();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        clientsTable = new JTable();
        showAccsBtn = new javax.swing.JButton();
        showclBtn = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        currAccTxt = new javax.swing.JLabel();
        deleteClient = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        delAccBtn = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        changeBtn = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        cnpTxt = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setText("Logged as: Admin");

        logoutBtn.setText("Logout");
        logoutBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                logoutBtnActionPerformed(evt);
            }
        });

        jLabel2.setText("Accounts:");

        accTable.setModel(
                new javax.swing.table.DefaultTableModel(
                        new Object[][]{{null, null, null, null}, {null, null, null, null},
                                {null, null, null, null}, {null, null, null, null}},
                        new String[]{"Title 1", "Title 2", "Title 3", "Title 4"}));
        jScrollPane1.setViewportView(accTable);

        jLabel3.setText("Users:");

        clientsTable
                .setModel(new javax.swing.table.DefaultTableModel(
                        new Object[][]{{null, null, null, null}, {null, null, null, null},
                                {null, null, null, null}, {null, null, null, null}},
                        new String[]{"Title 1", "Title 2", "Title 3", "Title 4"}));
        jScrollPane2.setViewportView(clientsTable);

        showAccsBtn.setText("Show Accounts");
        showAccsBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                showAccsBtnActionPerformed(evt);
            }
        });

        showclBtn.setText("Show Clients");
        showclBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                showclBtnActionPerformed(evt);
            }
        });

        jLabel4.setText("Current selected id:");

        deleteClient.setText("Delete");
        deleteClient.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteClientActionPerformed(evt);
            }
        });

        jLabel5.setText("Delete selected client from database:");

        jLabel6.setText("Delete selected account:");

        delAccBtn.setText("Delete");
        delAccBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                delAccBtnActionPerformed(evt);
            }
        });

        jLabel7.setText("Change Interest:");

        changeBtn.setText("Change");
        changeBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                changeBtnActionPerformed(evt);
            }
        });

        jLabel8.setText("Current selected CNP:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(layout
                .createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup().addGap(48, 48, 48)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup().addComponent(jLabel2)
                                                .addGap(18, 18, 18).addComponent(showAccsBtn)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED,
                                                        javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(jLabel1)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(logoutBtn))
                                        .addGroup(layout.createSequentialGroup().addGroup(layout
                                                .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addGroup(layout.createSequentialGroup().addComponent(jLabel4)
                                                        .addPreferredGap(
                                                                javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                        .addComponent(currAccTxt,
                                                                javax.swing.GroupLayout.PREFERRED_SIZE, 150,
                                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addGap(18, 18, 18).addComponent(jLabel8)
                                                        .addPreferredGap(
                                                                javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                        .addComponent(cnpTxt, javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                225, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 757,
                                                        javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGroup(layout.createSequentialGroup().addComponent(jLabel6)
                                                        .addPreferredGap(
                                                                javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                        .addComponent(delAccBtn))
                                                .addGroup(layout.createSequentialGroup().addComponent(jLabel7)
                                                        .addPreferredGap(
                                                                javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                        .addComponent(changeBtn)))
                                                .addGap(0, 0, Short.MAX_VALUE))))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup().addComponent(jLabel3)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(showclBtn))
                                        .addGroup(layout.createSequentialGroup().addComponent(jLabel5)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(deleteClient))
                                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 757,
                                                javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap()));
        layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup().addContainerGap()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(logoutBtn).addComponent(jLabel1)))
                                .addGroup(layout.createSequentialGroup().addGap(21, 21, 21)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(jLabel2).addComponent(showAccsBtn))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED,
                                javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(
                                jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 111,
                                javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(39, 39, 39)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(jLabel4).addComponent(currAccTxt,
                                                        javax.swing.GroupLayout.PREFERRED_SIZE, 14,
                                                        javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(5, 5, 5)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(jLabel6).addComponent(delAccBtn))
                                        .addGap(28, 28, 28)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(jLabel7).addComponent(changeBtn))
                                        .addGap(34, 34, 34)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(jLabel3).addComponent(showclBtn))
                                        .addGap(12, 12, 12)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(jLabel5).addComponent(deleteClient)))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel8).addComponent(cnpTxt,
                                                javax.swing.GroupLayout.PREFERRED_SIZE, 14,
                                                javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(49, 49, 49).addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 102,
                                javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(189, 189, 189)));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void logoutBtnActionPerformed(java.awt.event.ActionEvent evt) {

        WelcomeFrame w = new WelcomeFrame();
        w.setVisible(true);
        w.pack();
        this.setVisible(false);
    }

    private void showAccsBtnActionPerformed(java.awt.event.ActionEvent evt) {

        myAccounts = new ArrayList<Account>(300);

        Bank b = new Bank();
        b = b.restoreAccounts("accounts.txt");
        for (Iterator it = b.getAccounts().keySet().iterator(); it.hasNext(); ) {
            Client key = (Client) it.next();
            myAccounts.addAll(b.getAccounts(key));
        }
        createAccountsModel(myAccounts);

        b = b.restoreAccounts("spendings.txt");
        for (Iterator it = b.getAccounts().keySet().iterator(); it.hasNext(); ) {
            Client key = (Client) it.next();
            myAccounts.addAll(b.getAccounts(key));
        }
        accTable.setModel(accModel);
        accTable.setAutoCreateRowSorter(true);

        ChangeName(accTable, 0, "ID");
        ChangeName(accTable, 1, "Amount");
        ChangeName(accTable, 2, "First Name");
        ChangeName(accTable, 3, "Last Name");
        ChangeName(accTable, 4, "Date");
        ChangeName(accTable, 5, "Type");

    }

    private void showclBtnActionPerformed(java.awt.event.ActionEvent evt) {

        ClientProc c = new ClientProc();
        c = c.restoreClients();
        ArrayList<Client> myClients = c.getClients();
        createClientsModel(myClients);
        clientsTable.setModel(clientsModel);
        clientsTable.setAutoCreateRowSorter(true);
        ChangeName(clientsTable, 0, "CNP");
        ChangeName(clientsTable, 1, "User Name");
        ChangeName(clientsTable, 2, "First Name");
        ChangeName(clientsTable, 3, "Last Name");
        ChangeName(clientsTable, 4, "Address");
        this.addListener(clientsTable, 1);
    }

    /**
     * Sterge clientul si toate conturile sale
     *
     * @param evt
     */
    private void deleteClientActionPerformed(java.awt.event.ActionEvent evt) {

        // Cautam conturile clientului in cele 2 fisiere de conturi si unde il gasim il
        // stergem
        // Updatam in ambele cazuri fisierul cu obiectul nou
        Bank b = new Bank();
        Client client = new ClientProc().restoreClients().searchByCnp(currCNP);
        b = b.restoreAccounts("accounts.txt");
        if (b.getAccounts(client) != null) {
            b.removeClient(client);
            b.updateAccounts(b, "accounts.txt");
        }
        b = b.restoreAccounts("spendings.txt");
        if (b.getAccounts(client) != null) {
            b.removeClient(client);
            b.updateAccounts(b, "spendings.txt");
        }

        // Stergem si clientul
        ClientProc c = new ClientProc();
        c = c.restoreClients();
        c.deleteClient(c.searchByCnp(currCNP));
        c.updateClients(c);
        // Updatam tabelul
        ArrayList<Client> myClients = c.getClients();
        createClientsModel(myClients);
        clientsTable.setModel(clientsModel);
        clientsTable.setAutoCreateRowSorter(true);
        ChangeName(clientsTable, 0, "CNP");
        ChangeName(clientsTable, 1, "User Name");
        ChangeName(clientsTable, 2, "First Name");
        ChangeName(clientsTable, 3, "Last Name");
        ChangeName(clientsTable, 4, "Address");

    }

    /**
     * Sterge un cont
     *
     * @param evt
     */
    private void delAccBtnActionPerformed(java.awt.event.ActionEvent evt) {

        Bank b = new Bank();
        b = b.restoreAccounts("accounts.txt");
        Client client = new ClientProc().restoreClients().searchByCnp(currCNP);

        for (int i = 0; i < b.getAccounts(client).size(); i++) {
            if (b.getAccounts(client).get(i).getAccount() == currID) {
                b.removeAccount(client, b.getAccounts(client).get(i));
            }
        }
        b.updateAccounts(b, "accounts.txt");

        b = b.restoreAccounts("spendings.txt");

        for (int i = 0; i < b.getAccounts(client).size(); i++) {
            if (b.getAccounts(client).get(i).getAccount() == currID) {
                b.removeAccount(client, b.getAccounts(client).get(i));
            }
        }
        b.updateAccounts(b, "spendings.txt");

    }

    /**
     * Schimba dobanda unui cont
     *
     * @param evt
     */
    private void changeBtnActionPerformed(java.awt.event.ActionEvent evt) {
        ClientFrame cl = new ClientFrame();
        float current_interest = 0;
        float new_interest = Float.parseFloat(JOptionPane.showInputDialog(this, "Please insert the new interest"));

        Bank b = new Bank();
        b = b.restoreAccounts("accounts.txt");
        Client client = new ClientProc().restoreClients().searchByCnp(currCNP);

        ArrayList<Account> mysaves = b.getAccounts(client);
        for (int i = 0; i < mysaves.size(); i++) {
            if (mysaves.get(i).getAccount() == currID) {
                SavingAccount savingAccount = (SavingAccount) mysaves.get(i);
                JOptionPane.showMessageDialog(this, "You have succesfully changed interest for account with ID: "
                        + currID + "\n" + "From " + savingAccount.getInterest() + " to " + new_interest);

                savingAccount.setInterest(new_interest);
            }
        }
        b.updateAccounts(b, "accounts.txt");

        b = b.restoreAccounts("spendings.txt");
        ArrayList<SpendingAccount> mynewsaves = cl.convertSpending(b.getAccounts(client));
        for (int i = 0; i < mynewsaves.size(); i++) {
            if (mynewsaves.get(i).getAccount() == currID) {
                JOptionPane.showMessageDialog(this, "You have succesfully changed interest for account with ID: "
                        + currID + "\n" + "From " + mynewsaves.get(i).getRata() + " to " + new_interest);
                mynewsaves.get(i).setRata(new_interest);

            }
        }
        b.updateAccounts(b, "spendings.txt");

    }

    /**
     * Creeaza modelul de date pentru tabelul de conturi. Populeaza in fiecare camp
     * date despre contul curent
     *
     * @param accounts
     */
    public void createAccountsModel(final ArrayList<Account> accounts) {

        accModel = new AbstractTableModel() {

            @Override
            public int getColumnCount() {
                return 6;
            }

            @Override
            public int getRowCount() {
                return accounts.size();
            }

            @Override
            public Object getValueAt(int row, int col) {
                if (col == 0) {
                    int x = accounts.get(row).getNumber();
                    return new Integer(x);
                } else if (col == 1) {
                    double s = accounts.get(row).amount;
                    return new Float(s);
                } else if (col == 2) {
                    return accounts.get(row).getOwner().getFirstName();
                } else if (col == 3) {
                    return accounts.get(row).getOwner().getLastName();
                } else if (col == 4) {
                    return accounts.get(row).getDate();
                } else if (col == 5) {
                    // Verificam tipul contului
                    if (accounts.get(row) instanceof SavingAccount)
                        return "Savings";
                    else
                        return "Spendings";
                } else {
                    return 0;
                }
            }
        };
    }

    public void createClientsModel(final ArrayList<Client> clients) {

        clientsModel = new AbstractTableModel() {

            @Override
            public int getColumnCount() {
                return 5;
            }

            @Override
            public int getRowCount() {
                return clients.size();
            }

            @Override
            public Object getValueAt(int row, int col) {
                if (col == 0) {
                    return clients.get(row).getCNP();
                } else if (col == 1) {
                    return clients.get(row).getUsrName();
                } else if (col == 2) {
                    return clients.get(row).getFirstName();
                } else if (col == 3) {
                    return clients.get(row).getLastName();
                } else if (col == 4) {
                    return clients.get(row).getAddress();
                } else {
                    return 0;
                }
            }
        };
    }

    public void ChangeName(JTable table, int col_index, String col_name) {
        table.getColumnModel().getColumn(col_index).setHeaderValue(col_name);
    }

    /**
     * Listener pentru click in randurile din tabelul de clienti si de conturi.
     * Asculta doar dupa clicks in coloana zero
     *
     * @param myTable
     * @param type
     */
    private void addListener(JTable myTable, final int type) {
        myTable.addMouseListener(new MouseAdapter() {

            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 1) {
                    JTable target = (JTable) e.getSource();
                    int row = target.getSelectedRow();
                    int column = target.getSelectedColumn();
                    // cand se da click in prima coloana, populam date despre ID-ul contului sau
                    // CNP-ul clientului
                    if (column == 0) {
                        if (type == 0) {
                            currID = Integer.parseInt(target.getModel().getValueAt(row, column).toString());
                            currAccTxt.setText(target.getModel().getValueAt(row, column).toString());
                        } else {
                            currCNP = target.getModel().getValueAt(row, column).toString();
                            cnpTxt.setText(target.getModel().getValueAt(row, column).toString());
                        }
                    }

                }
            }
        });
    }

}
