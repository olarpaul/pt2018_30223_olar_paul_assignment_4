package tp.bankaaccounts;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Clasa bank contine o hash table care stocheaza conturile. Hash table este
 * implementata folosind tehnica de inlatuire, asa ca este formata dintr-un
 * array de liste. Acesta contine, de asemnea, functiile necesare specifice
 * pentru a adauga, elimina, cauta elemente din tabelul hash. Are cateva functii
 * de cautare speciale care efectueaza cautarea unui element al tabelului dupa
 * cateva campuri speciale pe care le doreste utilizatorul.
 *
 * @author olarp
 * @inv isWellFormed()
 */
public class Bank implements BankProc, Serializable {

    private HashMap<Client, ArrayList<Account>> accounts;
    private int accNr;

    /**
     * Constructor
     */
    public Bank() {
        accounts = new HashMap<>();
        accNr = 0;
        assert isWellFormed();
    }

    /**
     * Adauga un cont in hash table
     *
     * @param c       Proprietarul contului
     * @param account Contul care urmeaza sa fie adaugat
     * @pre (c ! = null & & account ! = null)
     * @post getAccounts(c.getCNP ()).size() + 1
     */
    @Override
    public void addAccount(Client c, Account account) {

        assert (c != null && account != null) : "Preconditions not met!";
        assert isWellFormed();

        ArrayList<Account> values = accounts.get(c);

        if (values == null) {
            values = new ArrayList<>();
        }

        int mySize = values.size();
        if (!values.contains(account)) {
            values.add(account);
            accounts.put(c, values);
            accNr++;
        }

        assert isWellFormed();
        assert getAccounts(c).size() == mySize + 1;
    }

    /**
     * Obtine toate conturile pentru clienti
     *
     * @param client proprietarul contului
     * @return Returneaza un array de conturi personale
     * @pre cnp != null
     * @post @nochange
     */
    @Override
    public ArrayList<Account> getAccounts(Client client) {

        assert client != null : "The CNP param is null";
        return accounts.get(client);

    }

    /**
     * Transforma o lista in string a conturilor
     *
     * @param key
     * @return s
     * @pre (key ! = null & & accounts.get ( key)!=null)
     * @post @nochange
     */
    @Override
    public String listToString(String key) {
        assert key != null && accounts.get(key) != null : "No ACCOUNTS";
        assert isWellFormed();

        String s = "";
        Iterator i = accounts.get(key).iterator();
        while (i.hasNext()) {
            s = s + i.next().toString();
        }
        assert isWellFormed();
        return s;
    }

    /**
     * Sterge un client din banca
     *
     * @param cnp CNP-ul clientului care urmeaza sa fie sters
     * @pre (accounts ! = null & & accounts.get ( cnp) != null)
     * @post accounts.get(cnp).size()--
     */
  
	public void removeClient(Client client) {
        assert isWellFormed();
        assert (accounts != null && getAccounts(client) != null);

        accNr -= this.getAccounts(client).size();
        accounts.remove(client);

        for (ArrayList<Account> acc : accounts.values()) {
        	//System.out.println(acc);
            accounts.remove(acc);

        }

        assert isWellFormed();
    }

    /**
     * Sterge un cont din lista de conturi a unui client
     *
     * @param cnp
     * @param account
     * @pre accounts != null && accounts.get(cnp).contains(account) &&
     * accounts.get(cnp) != null;
     * @post accNr--
     */
    @Override
    public void removeAccount(Client c, Account account) {
        assert isWellFormed();
        assert (accounts != null);
        assert accounts.get(c) != null;
        assert accounts.get(c).contains(account);
        accounts.get(c).remove(account);
        accNr--;
        assert isWellFormed();

    }

    /**
     * Prelueaza conturile dintr-un fisier
     *
     * @param filename numele fisierului de unde luam contul
     * @return un obiect bank care tine conturile
     * @pre fis != null File exists
     * @post @noChange
     */
    @Override
    public Bank restoreAccounts(String filename) {
        assert isWellFormed();
        try {
            FileInputStream fis = new FileInputStream(filename);
            assert fis != null;
            ObjectInputStream ois = new ObjectInputStream(fis);
            // citim obiectul de bank din objectinputstream-ul creat din fisier
            Bank accounts = (Bank) ois.readObject();

            ois.close();
            assert isWellFormed();
            return accounts;

        } catch (Exception e) {
            System.out.println("Err retrieveing files" + e);
            return null;
        }

    }

    /**
     * Metoda cu care updatam conturile
     *
     * @param accs     Accounts to be saved
     * @param filename The name of the file to hold the accounts
     * @pre (filename ! = null) && (accs!=null)
     * @post @noChange
     */
    @Override
    public void updateAccounts(Bank accs, String filename) {

        assert (accs != null) && (filename != null);
        assert isWellFormed();
        try {
            // Creaza fileoutputstream in fisierul dat ca parametru
            FileOutputStream fos = new FileOutputStream(filename);

            // Creem un output stream din acel fileoutputstream
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            // Scriem obiectul in intregime in fisier
            oos.writeObject(accs);
            oos.close();
            assert isWellFormed();

        } catch (Exception e) {
            System.out.println("Err writing to file " + e);
        }
    }

    /**
     * Metoda care returneaza conturile din hashmap
     *
     * @return
     */
    @Override
    public HashMap getAccounts() {
        return this.accounts;
    }

    /**
     * Metoda care verifica starea bine formata, este un invariant( adica este si precond si postcond)
     *
     * @return returneaza true daca hash table-ul este bine format, fals altfel
     */
    private boolean isWellFormed() {
        boolean ok = true;

        // numărul total de conturi egal cu suma mărimii listelor
        int totalsize = 0;
        for (Iterator it = accounts.keySet().iterator(); it.hasNext(); ) {
            Client key = (Client) it.next();
            totalsize += this.getAccounts(key).size();
        }
        // daca size-ul obtinut prin parcurgerea listei de conturi este diferit,
        // obiectul nu mai este bine format
        if (totalsize != accNr)
            ok = false;

        // Toate conturile trebuie sa aiba un owner
        Client c = null;
        for (Iterator it = accounts.keySet().iterator(); it.hasNext(); ) {
            Client key = (Client) it.next();
            for (int j = 0; j < accounts.get(key).size(); j++) {
                c = accounts.get(key).get(j).getOwner();
                if (c == null)
                    ok = false;
            }
        }

        return ok;
    }

}
