package tp.bankaaccounts;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Interfata care declara metodele ce urmeaza a fi implementate 
 *
 * 
 * @inv isWellFormed()
 */
public interface BankProc {

    /**
     * Metoda care adauga un cont
     *
     * @param c    
     * @param account 
     * @pre (c ! = null & & account ! = null)
     * @post getAccounts(c.getCNP ()).size() + 1
     */
    public void addAccount(Client c, Account account);

    /**
     * Obtine toate conturile unor clienti 
     *
     * @param 
     * @return 
     * @pre cnp != null
     * @post @nochange
     */
    public ArrayList<Account> getAccounts(Client client);

    /**
     * Metoda de sterge a unui client 
     *
     * @param 
     * @pre (accounts ! = null & & accounts.get ( cnp) != null)
     * @post accounts.get(cnp).size()--
     */
    public void removeClient(Client client);

    /**
     * Sterge un cont din lista de conturi a unui client 
     *
     * @param cnp     
     * @param account 
     * @pre accounts != null && accounts.get(cnp).contains(account) && accounts.get(cnp) != null;
     * @post accNr--
     */
    public void removeAccount(Client c, Account account);

    /**
     * Obtine conturile dintr-un fisier
     *
     * @param filename 
     * @return 
     * @pre fis != null File exists
     * @post @noChange
     */
    public Bank restoreAccounts(String filename);

    /**
     * Metoda care face update la un cont
     *
     * @param accs     
     * @param filename 
     * @pre
     * @post
     */
    public void updateAccounts(Bank accs, String filename);

    
    public HashMap getAccounts();

    /**
     * Transforma o lista in string a conturilor 
     *
     * @param key 
     * @return s 
     * @pre (key ! = null & & accounts.get ( key)!=null)
     * @post @nochange
     */
    public String listToString(String key);
}
