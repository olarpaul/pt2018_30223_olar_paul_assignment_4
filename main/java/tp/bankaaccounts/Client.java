package tp.bankaaccounts;

import javax.swing.*;
import java.io.Serializable;
import java.util.Observable;
import java.util.Observer;

/**
 * Aceasta clasa reprezinta abstractizarea conceptului de client al bancii.
 * Atributele necesare acestei calse sunt CNP unuice,
 * numele,prenumele,adresa,numele de utilizator si parola. De vreme ce vom avea
 * nevoie sa stocam clientii intr-un ArrayList pentru a putea sa ii accesam cand
 * dorim sa rerulam programul, aceasta clasa implementeaza o super clasa
 * serializable
 * Mai implementeaza si observer pentru a asculta evenimente de update ale unui cont ale clientului curent si a face anumite actiuni
 * 
 * @author olarp
 */
public class Client implements Serializable, Observer {

    public static final int ADAUGARE_BANI = 1;
    public static final int RETRAGERE_BANI = 2;
    public static final int SCHIMBARE_PERIOADA = 3;

    protected String lastName;
    protected String firstName;
    protected String address;
    protected String cnp;
    protected String usrName;
    protected String passwd;

    public Client() {
    }

    /**
     * Constructor de initalizare a username-ului si a parolei.
     *
     * @param userName
     * @param passwd
     */
    public Client(String userName, String passwd) {
        this.usrName = userName;
        this.passwd = passwd;
    }

    /**
     * Constructor folosit in momentul creierii unui cont nou.
     *
     * @param lastName
     * @param firstName
     * @param address
     * @param cnp
     * @param usrName
     * @param passwd
     */
    public Client(String lastName, String firstName, String address, String cnp, String usrName, String passwd) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.address = address;
        this.cnp = cnp;
        this.usrName = usrName;
        this.passwd = passwd;
    }

    /**
     * Metoda suprascrisa care returneaza numele, prenumele si adresa
     */
    @Override
    public String toString() {
        return "Name: " + firstName + " " + lastName + "\tAdrress: " + address + "\tMoney: ";
    }

    public String getFirstName() {
        return this.firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public String getAddress() {
        return this.address;
    }

    public String getCNP() {
        return this.cnp;
    }

    public String getUsrName() {
        return this.usrName;
    }

    public String getPass() {
        return this.passwd;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((cnp == null) ? 0 : cnp.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Client other = (Client) obj;
        if (cnp == null) {
            if (other.cnp != null)
                return false;
        } else if (!cnp.equals(other.cnp))
            return false;
        return true;
    }
    /**
     * Metoda care are 2 parametrii : 1 observable-ul si al doilea este tipul de eveniment
     */
    @Override
    public void update(Observable arg0, Object arg1) {
        // Aici ascultam pentru updates de la observable items.
        // Deoarece clientul este un observer el va fi notificat de fiecare data cand apare o schimbare intr-un observable

        // Contul in care s-a produs o schimbare
        Account account = (Account) arg0;

        // Perluam tipul schimbarii si luam actiunea potrivita
        int type = (int) arg1;
        switch (type) {
            // In functie de actiune afisam un mesaj corespunzator utilizatorului
            case ADAUGARE_BANI:
                JOptionPane.showMessageDialog(null, "Felicitari, " + firstName + " " + lastName
                        + "! S-au adaugat bani in contul: " + account.number + "!");
                break;
            case RETRAGERE_BANI:
                JOptionPane.showMessageDialog(null, "Felicitari, " + firstName + " " + lastName
                        + "! S-au retras bani din contul: " + account.number + "!");
                break;
            case SCHIMBARE_PERIOADA:
                JOptionPane.showMessageDialog(null, "Felicitari, " + firstName + " " + lastName
                        + "! S-a schimbat perioada depozitului in contul: " + account.number + "!");
                break;
        }
        System.out.println("Contul tau a fost schimbat: " + account.number);
    }

}
