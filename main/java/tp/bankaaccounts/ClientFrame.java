package tp.bankaaccounts;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.*;

/**
 * Clasa care detine extinde un frame si permite clientului sa intercioneze cu
 * sistemul
 *
 * @author olarp
 */
public final class ClientFrame extends javax.swing.JFrame {

    private static final int SAVING = 0;
    private static final int SPENDING = 1;
    private Client currClient;
    private TableModel accModel, spendModel;
    private ArrayList<Account> currAccounts;
    private int currID = 0;
    private javax.swing.JTextField CNPtxt;
    private JTable accTable;
    private javax.swing.JButton addBtn;
    private javax.swing.JTextField addrTxt;
    private javax.swing.JButton changeBtn;
    private javax.swing.JButton createbtn;
    private javax.swing.JTextField currAccTxt;
    private javax.swing.JButton deleteBtn;
    private javax.swing.JTextField firstNtxt;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextField lastNtxt;
    private javax.swing.JButton showBtn;
    private JTable spendTable;
    private javax.swing.JComboBox typeCmb;
    private javax.swing.JLabel userTxt;
    private javax.swing.JButton withdrBtn;

    public ClientFrame() {
        initComponents();
        currClient = restoreClient();
        userTxt.setText(currClient.getUsrName());
        firstNtxt.setText(currClient.getFirstName());
        lastNtxt.setText(currClient.getLastName());
        CNPtxt.setText(currClient.getCNP());
        addrTxt.setText(currClient.getAddress());

        this.addListener(spendTable);
        this.addListener(accTable);
        this.setName(0);
        this.setName(1);

    }

    @SuppressWarnings("unchecked")

    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        userTxt = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        addrTxt = new javax.swing.JTextField();
        lastNtxt = new javax.swing.JTextField();
        CNPtxt = new javax.swing.JTextField();
        firstNtxt = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        accTable = new JTable();
        createbtn = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        typeCmb = new javax.swing.JComboBox();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        spendTable = new JTable();
        withdrBtn = new javax.swing.JButton();
        jLabel12 = new javax.swing.JLabel();
        currAccTxt = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        addBtn = new javax.swing.JButton();
        jLabel15 = new javax.swing.JLabel();
        changeBtn = new javax.swing.JButton();
        jLabel16 = new javax.swing.JLabel();
        deleteBtn = new javax.swing.JButton();
        showBtn = new javax.swing.JButton();


        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel2.setText("Logged as:");

        userTxt.setText("jLabel3");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel3.setText("Profile Details:");

        jLabel5.setText("Address:");

        jLabel4.setText("CNP:");

        jLabel6.setText("Last Name:");

        jLabel7.setText("First Name:");

        jButton1.setText("Logout");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel1.setText("Current Accounts:");

        accTable.setModel(new javax.swing.table.DefaultTableModel(
                new Object[][]{{null, null, null, null, null, null, null},
                        {null, null, null, null, null, null, null}, {null, null, null, null, null, null, null},
                        {null, null, null, null, null, null, null}},
                new String[]{"ID", "Amount Deposited", "Date Deposited", "Interest", "Saving Period", "Saved",
                        "Total"}));
        jScrollPane1.setViewportView(accTable);

        createbtn.setText("Create");
        createbtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                createbtnActionPerformed(evt);
            }
        });

        jLabel8.setText("Create New Account:");

        typeCmb.setModel(new javax.swing.DefaultComboBoxModel(new String[]{"Saving", "Spending"}));

        jLabel9.setText("Account");

        jLabel10.setText("Saving Accounts");

        jLabel11.setText("Spending Accounts");

        spendTable.setModel(new javax.swing.table.DefaultTableModel(
                new Object[][]{{null, null, null, null, null, null, null},
                        {null, null, null, null, null, null, null}, {null, null, null, null, null, null, null},
                        {null, null, null, null, null, null, null}},
                new String[]{"ID", "Title 2", "Title 3", "Title 4", "Title 5", "Title 6", "Title 7"}));
        jScrollPane2.setViewportView(spendTable);

        withdrBtn.setText("Withdraw");
        withdrBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                withdrBtnActionPerformed(evt);
            }
        });

        jLabel12.setText("Withdraw from selected account:");

        jLabel13.setText("Current account:");

        jLabel14.setText("Add money to selected account:");

        addBtn.setText("Add");
        addBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addBtnActionPerformed(evt);
            }
        });

        jLabel15.setText("Change period to selected account:");

        changeBtn.setText("Change");
        changeBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                changeBtnActionPerformed(evt);
            }
        });

        jLabel16.setText("Delete selected account:");

        deleteBtn.setText("Delete");
        deleteBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteBtnActionPerformed(evt);
            }
        });

        showBtn.setText("Show Accounts");
        showBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                showBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(layout
                .createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup().addGap(31, 31, 31).addGroup(layout
                                .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(jLabel3)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addGroup(layout.createSequentialGroup().addComponent(jLabel4)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(CNPtxt))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
                                                layout.createSequentialGroup().addComponent(jLabel6)
                                                        .addPreferredGap(
                                                                javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                        .addComponent(lastNtxt))
                                        .addGroup(layout.createSequentialGroup().addComponent(jLabel5)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(addrTxt))
                                        .addGroup(layout.createSequentialGroup().addComponent(jLabel7)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(firstNtxt, javax.swing.GroupLayout.PREFERRED_SIZE, 170,
                                                        javax.swing.GroupLayout.PREFERRED_SIZE)))))
                        .addGroup(layout.createSequentialGroup().addContainerGap().addComponent(jLabel14))
                        .addGroup(layout.createSequentialGroup().addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel15).addComponent(jLabel16))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(deleteBtn).addComponent(changeBtn))))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 700,
                                        Short.MAX_VALUE)
                                .addComponent(jButton1).addGap(18, 18, 18))
                        .addGroup(layout.createSequentialGroup().addGap(44, 44, 44).addGroup(layout
                                .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 739, Short.MAX_VALUE)
                                .addGroup(layout.createSequentialGroup().addComponent(jLabel1).addGap(191, 191, 191)
                                        .addComponent(jLabel10).addGap(0, 0, Short.MAX_VALUE))
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING)))))
                .addGroup(layout.createSequentialGroup().addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
                                        layout.createSequentialGroup().addGap(0, 0, Short.MAX_VALUE)
                                                .addComponent(showBtn).addGap(380, 380, 380).addComponent(jLabel2)
                                                .addPreferredGap(
                                                        javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(userTxt).addContainerGap())
                                .addGroup(layout.createSequentialGroup().addGroup(layout
                                        .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel8).addComponent(jLabel12)
                                        .addGroup(layout.createSequentialGroup().addComponent(jLabel13)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(currAccTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 79,
                                                        javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addGroup(layout.createSequentialGroup().addGap(
                                                        0, 0, Short.MAX_VALUE).addComponent(jLabel11)
                                                        .addGap(333, 333, 333))
                                                .addGroup(layout.createSequentialGroup().addGroup(layout
                                                        .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(withdrBtn)
                                                        .addGroup(layout.createSequentialGroup().addComponent(
                                                                typeCmb, javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addPreferredGap(
                                                                        javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                                .addComponent(jLabel9)
                                                                .addPreferredGap(
                                                                        javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                .addComponent(createbtn))
                                                        .addComponent(addBtn)).addGap(0, 0, Short.MAX_VALUE))))
                                .addGroup(layout.createSequentialGroup()

                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)

                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)

                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)

                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)

                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)

                                        .addGap(0, 0, Short.MAX_VALUE)))));
        layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(layout
                .createSequentialGroup().addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING).addGroup(layout
                        .createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(jLabel2).addComponent(userTxt))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jButton1).addGap(8, 8, 8)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 17,
                                                        javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(jLabel10)))
                                .addComponent(showBtn))
                        .addGap(18, 18, 18).addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 92,
                                javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createSequentialGroup().addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel7).addComponent(firstNtxt,
                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel6).addComponent(lastNtxt,
                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel4).addComponent(CNPtxt,
                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel5).addComponent(addrTxt,
                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(36, 36, 36)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE).addComponent(createbtn)
                        .addComponent(jLabel8)
                        .addComponent(typeCmb, javax.swing.GroupLayout.PREFERRED_SIZE,
                                javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel9))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE).addComponent(withdrBtn)
                        .addComponent(jLabel12))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 9, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE).addComponent(jLabel11)
                        .addComponent(currAccTxt, javax.swing.GroupLayout.PREFERRED_SIZE,
                                javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel13))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 94,
                                javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel14).addComponent(addBtn))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel15).addComponent(changeBtn))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel16).addComponent(deleteBtn))))
                .addGap(18, 18, 18)

        ));

        pack();
    }

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {
        WelcomeFrame w = new WelcomeFrame();
        w.setVisible(true);
        w.pack();
        this.setVisible(false);
    }

    /**
     * Adauga cont, in functie de tipul selectat in combo box, vom adauga un savings
     * account sau un spendings account
     *
     * @param evt
     */
    private void createbtnActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_createbtnActionPerformed
        Random r = new Random();
        Account myAccount;
        Calendar rightNow = Calendar.getInstance();

        if (typeCmb.getSelectedItem() == "Saving") {

            int amount = Integer.parseInt(
                    JOptionPane.showInputDialog(this, "Please insert the amount of money you want to deposit"));
            int period = Integer.parseInt(JOptionPane.showInputDialog(this, "Please insert the saving period"));

            myAccount = new SavingAccount(r.nextInt(10000), currClient, amount, period, rightNow.getTime());
            Bank b = new Bank();

            SavingAccount mysaving = (SavingAccount) myAccount;

            if (period < 10) {
                mysaving.setInterest((float) 0.05);
            } else if (period >= 10 && period < 24) {
                mysaving.setInterest((float) 0.06);
            } else if (period >= 24) {
                mysaving.setInterest((float) 0.1);
            }

            mysaving.computeSaving();

            mysaving.getTotalAmount();
            b = b.restoreAccounts("accounts.txt");
            // Adaugam contul la clientul curent
            b.addAccount(currClient, mysaving);
            // Updatam fisierul
            b.updateAccounts(b, "accounts.txt");

            // Updatam tabelul de conturi
            if (b.getAccounts(currClient) != null) {
                createSavingsAccountModel(b.getAccounts(currClient));
                accTable.setModel(accModel);
            }


            setName(SAVING);

        } else {
            int amount = Integer.parseInt(
                    JOptionPane.showInputDialog(this, "Please insert the amount of money you want to borrow"));
            int period = Integer.parseInt(JOptionPane.showInputDialog(this, "Please insert the period to return"));
            myAccount = new SpendingAccount(r.nextInt(10000), currClient, amount, period, rightNow.getTime());

            Bank b = new Bank();

            SpendingAccount myspending = (SpendingAccount) myAccount;

            if (period < 10) {
                myspending.setRata((float) 0.05);
            } else if (period >= 10 && period < 24) {
                myspending.setRata((float) 0.06);
            } else if (period >= 24) {
                myspending.setRata((float) 0.1);
            }

            b = b.restoreAccounts("spendings.txt");
            b.addAccount(currClient, myspending);
            b.updateAccounts(b, "spendings.txt");

            createSpendingAccountsModel(convertSpending(b.getAccounts(currClient)));
            spendTable.setModel(spendModel);

            setName(SPENDING);

        }
    }

    /**
     * Handle la butonul de withdraw money
     *
     * @param evt
     */
    private void withdrBtnActionPerformed(java.awt.event.ActionEvent evt) {

        Bank b = new Bank();

        b = b.restoreAccounts("spendings.txt");
        ArrayList<SpendingAccount> mytmp = convertSpending(b.getAccounts(currClient));

        // Parcurgem lista de conturi si cautam contul cu id-ul curent
        for (int i = 0; i < mytmp.size(); i++) {
            if (mytmp.get(i).getAccount() == currID) {
                // Daca incercam sa scoatem bani dintr-un cont de spending, afisam mesaj de
                // eroare
                JOptionPane.showMessageDialog(this, "You cannot withdraw money from a spending account!");
                return;
            }
        }

        // Similar pentru conturile de savings
        b = b.restoreAccounts("accounts.txt");
        ArrayList<Account> tmp = b.getAccounts(currClient);
        float amount_withdraw = Float
                .parseFloat(JOptionPane.showInputDialog(this, "Please insert the amount to withdraw"));
        // Daca gasim contul, apelam metoda de retragere bani
        for (int i = 0; i < tmp.size(); i++) {
            if (tmp.get(i).getAccount() == currID) {
                SavingAccount savingAccount = (SavingAccount) tmp.get(i);
                // Adaugam clientul ca si observer la contul proaspat adaugat
                savingAccount.addObserver(currClient);
                savingAccount.withdrawMoney(amount_withdraw);
                savingAccount.getTotalAmount();
            }
        }
        // Updatam fisierul
        b.updateAccounts(b, "accounts.txt");
        // Updatam UI-ul
        restoreTable();

    }

    /**
     * Adauga bani in cont, fie el de savings sau de spendings
     *
     * @param evt
     */
    private void addBtnActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_addBtnActionPerformed
        // Cautam contul in cele 2 fisiere de conturi si adaugam bani la contul gasit.
        // Updatam fisierul si UI-ul
        Bank b = new Bank();
        b = b.restoreAccounts("accounts.txt");
        ArrayList<Account> tmp = b.getAccounts(currClient);
        float amount_add = Float
                .parseFloat(JOptionPane.showInputDialog(this, "Please insert the amount to add to your account"));
        // System.out.println(currID);
        for (int i = 0; i < tmp.size(); i++) {
            if (tmp.get(i).getAccount() == currID) {
                SavingAccount savingAccount = (SavingAccount) tmp.get(i);
                // Adaugam ca si observer clientul curent la contul proaspat updatat
                savingAccount.addObserver(currClient);
                savingAccount.depositMoney(amount_add);
                savingAccount.getTotalAmount();
            }
        }
        b.updateAccounts(b, "accounts.txt");
        restoreTable();

        b = b.restoreAccounts("spendings.txt");
        ArrayList<SpendingAccount> myAccs = convertSpending(b.getAccounts(currClient));

        for (int i = 0; i < myAccs.size(); i++) {
            if (myAccs.get(i).getAccount() == currID) {
                // Similar pentru contul de spendings adaugam ca si observer clientul curent la contul proaspat updatat
                myAccs.get(i).addObserver(currClient);
                myAccs.get(i).adaugaSuma(amount_add);
            }
        }
        b.updateAccounts(b, "spendings.txt");
        restoreTable();

    }

    /**
     * Schimba perioada la contul curent selectat
     *
     * @param evt
     */
    private void changeBtnActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_changeBtnActionPerformed
        // Cautam contul in cele 2 fisiere si schimbam perioada
        Bank b = new Bank();
        b = b.restoreAccounts("accounts.txt");
        ArrayList<Account> tmp = b.getAccounts(currClient);
        int period = Integer.parseInt(JOptionPane.showInputDialog(this, "Please insert the new period"));

        for (int i = 0; i < tmp.size(); i++) {
            if (tmp.get(i).getAccount() == currID) {
                SavingAccount savingAccount = (SavingAccount) tmp.get(i);
                // Adaugam ca si observer clientul pentru a fi notificat pentru schimbarea de perioada la contul curent
                savingAccount.addObserver(currClient);
                savingAccount.changePeriod(period);

                if (period < 10) {
                    savingAccount.setInterest((float) 0.05);
                } else if (period >= 10 && period < 24) {
                    savingAccount.setInterest((float) 0.06);
                } else if (period >= 24) {
                    savingAccount.setInterest((float) 0.1);
                }

                savingAccount.getTotalAmount();
            }
        }
        b.updateAccounts(b, "accounts.txt");
        restoreTable();

        b = b.restoreAccounts("spendings.txt");
        ArrayList<SpendingAccount> mytmp = convertSpending(b.getAccounts(currClient));
        for (int i = 0; i < mytmp.size(); i++) {
            if (mytmp.get(i).getAccount() == currID) {
                // Similar pentru cont de spendings, adaugam clientul ca observer
                mytmp.get(i).addObserver(currClient);
                mytmp.get(i).schimaPerioada(period);
            }
        }
        b.updateAccounts(b, "spendings.txt");
        restoreTable();

    }

    /**
     * Sterge contul curent selectat
     *
     * @param evt
     */
    private void deleteBtnActionPerformed(java.awt.event.ActionEvent evt) {
        // Cautam contul in cele 2 obiecte din fisiere, le stergem si upodatam fisierele
        // si GUI-ul

        Bank b = new Bank();
        b = b.restoreAccounts("accounts.txt");

        for (int i = 0; i < b.getAccounts(currClient).size(); i++) {
            if (b.getAccounts(currClient).get(i).getAccount() == currID) {
                b.removeAccount(currClient, b.getAccounts(currClient).get(i));
            }
        }
        b.updateAccounts(b, "accounts.txt");
        restoreTable();

        b = b.restoreAccounts("spendings.txt");

        for (int i = 0; i < b.getAccounts(currClient).size(); i++) {
            if (b.getAccounts(currClient).get(i).getAccount() == currID) {
                b.removeAccount(currClient, b.getAccounts(currClient).get(i));
            }
        }
        b.updateAccounts(b, "spendings.txt");
        restoreTable();

    }

    /**
     * Populeaza tabelul de conturi cu informatii luate din fisier
     *
     * @param evt
     */
    private void showBtnActionPerformed(java.awt.event.ActionEvent evt) {
        restoreTable();
        currAccounts = new ArrayList<Account>(300);

        Bank b = new Bank();
        b = b.restoreAccounts("accounts.txt");
        for (Iterator it = b.getAccounts().keySet().iterator(); it.hasNext(); ) {
            Client key = (Client) it.next();
            currAccounts.addAll(b.getAccounts(key));
        }

        b = b.restoreAccounts("spendings.txt");
        for (Iterator it = b.getAccounts().keySet().iterator(); it.hasNext(); ) {
            Client key = (Client) it.next();
            currAccounts.addAll(b.getAccounts(key));
        }

        String[] names = new String[currAccounts.size()];
        for (int i = 0; i < currAccounts.size(); i++) {
            names[i] = (currAccounts.get(i).getOwner().getUsrName());
        }

        names = this.removeDuplicates(names);
    }

    private Client restoreClient() {
        try {
            FileInputStream fis = new FileInputStream("currentClient.txt");
            ObjectInputStream ois = new ObjectInputStream(fis);
            Client client = (Client) ois.readObject();

            ois.close();
            return client;

        } catch (Exception e) {
            System.out.println("Err retrieveing files" + e);
            return null;
        }
    }

    public void createSavingsAccountModel(final ArrayList<Account> accounts) {

        accModel = new AbstractTableModel() {

            @Override
            public int getColumnCount() {
                return 7;
            }

            @Override
            public int getRowCount() {
                return accounts.size();
            }

            @Override
            public Object getValueAt(int row, int col) {
                SavingAccount savingAccount = (SavingAccount) accounts.get(row);
                if (col == 0) {
                    int x = savingAccount.getNumber();

                    return new Integer(x);
                } else if (col == 1) {
                    double s = savingAccount.amount;
                    return new Float(s);
                } else if (col == 2) {
                    return savingAccount.getDate();
                } else if (col == 3) {

                    float x = savingAccount.getInterest();
                    return new Float(x);
                } else if (col == 4) {

                    int x = savingAccount.getPeriod();
                    return new Integer(x);
                } else if (col == 5) {
                    double x = savingAccount.getTotal() - accounts.get(row).amount;
                    return new Double(x);
                } else if (col == 6) {
                    double x = savingAccount.getTotal();
                    return new Double(x);
                } else {
                    return 0;
                }
            }
        };

    }

    public void createSpendingAccountsModel(final ArrayList<SpendingAccount> accounts) {

        spendModel = new AbstractTableModel() {

            @Override
            public int getColumnCount() {
                return 7;
            }

            @Override
            public int getRowCount() {
                return accounts.size();
            }

            @Override
            public Object getValueAt(int row, int col) {
                if (col == 0) {
                    int x = accounts.get(row).getNumber();

                    return new Integer(x);
                } else if (col == 1) {
                    double s = accounts.get(row).amount;
                    return new Float(s);
                } else if (col == 2) {
                    return accounts.get(row).getDate();
                } else if (col == 3) {

                    float x = accounts.get(row).getRata();
                    return new Float(x);
                } else if (col == 4) {

                    int x = accounts.get(row).getPerioada();
                    return new Integer(x);
                } else if (col == 5) {
                    double x = accounts.get(row).calculeazaRata();
                    return new Double(x);
                } else if (col == 6) {
                    double x = accounts.get(row).calculate_total();
                    return new Double(x);
                } else {
                    return 0;
                }
            }
        };

    }

    public void ChangeName(JTable table, int col_index, String col_name) {
        table.getColumnModel().getColumn(col_index).setHeaderValue(col_name);
    }

    public void restoreTable() {
        Bank b = new Bank();
        b = b.restoreAccounts("accounts.txt");

        createSavingsAccountModel(b.getAccounts(currClient));
        accTable.setModel(accModel);
        setName(SAVING);

        b = b.restoreAccounts("spendings.txt");
        createSpendingAccountsModel(convertSpending(b.getAccounts(currClient)));
        spendTable.setModel(spendModel);
        setName(SPENDING);

    }

//    public ArrayList<SavingAccount> convertArray(ArrayList<Account> myArray) {
//        ArrayList<SavingAccount> tmp = new ArrayList<>(myArray.size());
//        for (int i = 0; i < myArray.size(); i++) {
//            tmp.add(i, (SavingAccount) myArray.get(i));
//        }
//        return tmp;
//    }

    public ArrayList<SpendingAccount> convertSpending(ArrayList<Account> myArray) {
        ArrayList<SpendingAccount> tmp = new ArrayList<>(myArray.size());
        for (int i = 0; i < myArray.size(); i++) {
            tmp.add(i, (SpendingAccount) myArray.get(i));
        }
        return tmp;
    }

    private void addListener(JTable myTable) {
        myTable.addMouseListener(new MouseAdapter() {

            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 1) {
                    JTable target = (JTable) e.getSource();
                    int row = target.getSelectedRow();
                    int column = target.getSelectedColumn();
                    if (column == 0) {
                        currID = Integer.parseInt(target.getModel().getValueAt(row, column).toString());
                        currAccTxt.setText(target.getModel().getValueAt(row, column).toString());
                    }

                }
            }
        });
    }

    private void setName(int type) {
        if (type == SAVING) {
            ChangeName(accTable, 0, "ID");
            ChangeName(accTable, 1, "Amount Deposited");
            ChangeName(accTable, 2, "Date Deposited");
            ChangeName(accTable, 3, "Interest");
            ChangeName(accTable, 4, "Saving Period");
            ChangeName(accTable, 5, "Saved");
            ChangeName(accTable, 6, "Total");
        } else if (type == SPENDING) {
            ChangeName(spendTable, 0, "ID");
            ChangeName(spendTable, 1, "Amount Borrowed");
            ChangeName(spendTable, 2, "Date Borrowed");
            ChangeName(spendTable, 3, "Interest");
            ChangeName(spendTable, 4, "Return Period");
            ChangeName(spendTable, 5, "Rate/Month");
            ChangeName(spendTable, 6, "Total To Be Returned");
        }
    }

    public String[] removeDuplicates(String[] al) {
        List<String> list = Arrays.asList(al);
        Set<String> set = new HashSet<String>(list);
        String[] result = new String[set.size()];
        set.toArray(result);
        return result;
    }

}
