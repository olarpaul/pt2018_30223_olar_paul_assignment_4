package tp.bankaaccounts;

import java.io.*;
import java.util.ArrayList;

/**
 * Clasa care stocheaza obiecte de tip client intr-un ArrayList, pentru a fi mai
 * usor de accesat.De asemenea, implementeaza serializable
 * 
 * @author olarp
 *
 */
public class ClientProc implements Serializable {

	private static final int MAX_CLIENTS = 1000;
	private ArrayList<Client> myClients;

	/**
	 * Constructor care intializeaza un ArrayList de clienti
	 */
	public ClientProc() {
		myClients = new ArrayList<>(MAX_CLIENTS);
	}

	/**
	 * Metoda folosita pentru adaugare a unui client listei
	 * 
	 * @param c
	 */
	public void addClient(Client c) {
		myClients.add(c);
	}

	/**
	 * Metoda de sterge a unui client din lista
	 * 
	 * @param c
	 */
	public void removeClient(Client c) {
		myClients.remove(c);
	}

	/**
	 * Metoda pentru cautare a unui client din lista
	 * 
	 * @param c
	 * @return
	 */
	public Client searchClient(Client c) {
		Client result = new Client();
		for (int i = 0; i < myClients.size(); i++) {
			// Verificam clientul dupa username si parola, daca l-am gasit salvam in
			// variabila result
			if (myClients.get(i).getUsrName().equals(c.getUsrName())
					&& myClients.get(i).getPass().equals(c.getPass())) {
				result = myClients.get(i);
			}
		}
		return result;
	}

	/**
	 * Metoda de cautare a unui client din lista dupa CNP
	 * 
	 * @param CNP
	 * @return
	 */
	public Client searchByCnp(String CNP) {
		Client result = null;
		for (int i = 0; i < myClients.size(); i++) {
			if (myClients.get(i).getCNP().equals(CNP)) {
				result = myClients.get(i);
			}
		}
		return result;
	}

	/**
	 * Metoda pentru sterge a unui client
	 * 
	 * @param c
	 */
	public void deleteClient(Client c) {

		for (int i = 0; i < myClients.size(); i++) {
			if (myClients.get(i) == c) {
				myClients.remove(i);
			}
		}
	}

	/**
	 * Metoda folosita pentru restabili clientii dintr-un fisier.
	 * 
	 * @return
	 */
	public ClientProc restoreClients() {
		try {
			FileInputStream fis = new FileInputStream("clients.txt");
			ObjectInputStream ois = new ObjectInputStream(fis);
			ClientProc clients = (ClientProc) ois.readObject();

			ois.close();
			return clients;

		} catch (Exception e) {
			System.out.println("Err retrieveing files" + e);
			return null;
		}
	}

	/**
	 * Metoda folosită pentru a scrie într-un fișier obiectul ClientProc care
	 * conține clienții
	 * 
	 * @param clients
	 */
	public void updateClients(ClientProc clients) {
		try {
			FileOutputStream fos = new FileOutputStream("clients.txt");

			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(clients);
			oos.close();

		} catch (Exception e) {
			System.out.println("Err writing to file " + e);
		}
	}

	/**
	 * Metoda care returneaza clinetii din lista
	 * 
	 * @return
	 */
	public ArrayList<Client> getClients() {
		return this.myClients;
	}
}
