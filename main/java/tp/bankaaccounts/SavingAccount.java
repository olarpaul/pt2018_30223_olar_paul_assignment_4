package tp.bankaaccounts;

import java.util.Date;

/**
 * Clasa SavingAccount reprezinta un cont de economii si are unele metode
 * speciale pentru a efectuarea operatiilor necesare unui cont de acest tip.
 * Clasa contine metode pentru a simula extractia, adaugarea unei sume de bani
 * precum si calcularea dobanzii. Aceasta clasa mostente clasa Account
 *
 * @author olarp
 */
public class SavingAccount extends Account {

    protected int perioada; // definit in luni
    protected float dobanda;
    protected double total;

    /**
     * Constructor pentru a seta informatiile transmise ca parametru pentru un cont
     * de economii
     *
     * @param id
     * @param p
     * @param suma
     * @param per
     * @param date
     */
    public SavingAccount(int id, Client p, float suma, int per, Date date) {
        super(id, p, suma, date);
        this.perioada = per;
        dobanda = (float) 0.05;
        total = 0;
    }

    /**
     * Metoda care calculeaza banii ce urmeaza a fii salvati
     *
     * @return
     */
    public float computeSaving() {
        // rata este de rata% iar cu fiecare luna in plus creste cu cate rata/10%
        return (float) (dobanda * this.amount + (perioada - 1) * dobanda * this.amount / 10);
    }

    /**
     * Metoda folosita pentru a retrage bani dintr-un cont de econmii.
     *
     * @param s
     * @return
     */
    public boolean withdrawMoney(float s) {
        if (amount >= s) {
            amount = amount - s;
            // Notificam observerul ca o retragere de bani s-a produs pe cont
            setChanged();
            notifyObservers(Client.RETRAGERE_BANI);
            return true;
        }
        return false;
    }

    /**
     * Metoda pentru depozitare a banilor
     *
     * @param a
     */
    public void depositMoney(float a) {
        this.amount = this.amount + a;
        // Notificam observerul(clientul) ca s-au adaugat bani in cont
        setChanged();
        notifyObservers(Client.ADAUGARE_BANI);
    }

    /**
     * Metoda de schimbare a perioadei
     *
     * @param per
     */
    public void changePeriod(int per) {
        perioada = per;
        // Notificam observerul (clientul) ca s-a schimbat perioada contului curent
        setChanged();
        notifyObservers(Client.SCHIMBARE_PERIOADA);
    }

    /**
     * Calculeaza totalul banilor
     */
    public void getTotalAmount() {
        this.total = this.amount + this.computeSaving();
    }

    /**
     * Metoda care returneaza un string a contului de economii
     *
     * @return
     */
    public String whoAreYou() {
        return "Contul " + super.number + " is Depozit";
    }

    public float getInterest() {
        return dobanda;
    }

    public void setInterest(float a) {
        dobanda = a;
    }

    public int getPeriod() {
        return perioada;
    }

    public double getTotal() {
        return this.total;

    }
}
