package tp.bankaaccounts;

import java.util.Date;

/**
 * Clasa SpendingAccount simuleaza un cont de "spending", implementeaza metode
 * similare cu cele din clasa "SavingAccount"
 *
 * @author olarp
 */
class SpendingAccount extends Account {

    protected float rata;
    protected int perioada;

    /**
     * Constructor ce seteaza valoriile initaliale
     *
     * @param id
     * @param p
     * @param suma
     * @param perioada
     * @param date
     */
    public SpendingAccount(int id, Client p, float suma, int perioada, Date date) {
        super(id, p, suma, date);
        rata = (float) 0.06;
        this.perioada = perioada;
    }

    public boolean withdrawMoney(float s) {
        return false;
    }

    public double calculate_total() {
        return super.amount + super.amount * this.rata;
    }

    public void schimaPerioada(int per) {
        this.perioada = per;
        // Notificam clientul ca s-a updatat perioada contului
        setChanged();
        notifyObservers(Client.SCHIMBARE_PERIOADA);
    }

    public String whoAreYou() {
        return "Contul " + super.number + " este Rata";
    }

    public double calculeazaRata() {
        // rata este de rata% iar cu fiecare luna in plus creste cu cate rata/5%
        return (super.amount / this.perioada) * this.rata + (super.amount / this.perioada);
    }

    public void adaugaSuma(float a) {
        this.amount = this.amount - a;
        // scadem din suma care mai trebuie returnata bancii
        System.out.println("Should notify observers");
        // Notificam observerul
        setChanged();
        notifyObservers(Client.ADAUGARE_BANI);
    }

    public float getRata() {
        return rata;
    }

    public void setRata(float a) {
        rata = a;
    }

    public int getPerioada() {
        return this.perioada;
    }

}
