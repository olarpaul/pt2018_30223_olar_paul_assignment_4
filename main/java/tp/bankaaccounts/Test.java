package tp.bankaaccounts;

/**
 * Clasa Start conține principala metodă numită atunci când aplicația este
 * rulată pentru prima dată.
 * 
 * @author olarp
 *
 */
public class Test {

	/**
	 * * Testul de clasă conține metoda principală de rulare a programului, apelând
	 * o instanță a programului WelcomeFrame.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		WelcomeFrame w = new WelcomeFrame();
		w.setVisible(true);
		
		/*Bank bank = new Bank();
		bank.updateAccounts(bank, "spendings.txt");
		bank.updateAccounts(bank, "accounts.txt");
		ClientProc clientProc = new ClientProc();
		clientProc.updateClients(clientProc);
		System.out.println("Update");*/
	}
}
