package tp.bankaaccounts;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.List;
import java.util.Random;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

/**
 * Clasa care testeaza functionalitatea clasei Bank
 */
@RunWith(JUnit4.class)
public class BankTest {

    /**
     * Testam constructorul clasei
     */

    @Test
    public void testBankCreate() {
        Bank b = new Bank();
        // O instanta noua a clasei bank nu ar trebui sa contina niciun cont
        assertEquals(0, b.getAccounts().size());
    }
    
    /**
     * Testam functionalitatea de stergere client impreuna cu toate conturile sale
     */
    @Test
    public void testRemoveClientAndAccounts() {
        // Creem o banca, cont si adaugam conturi
        Bank b = new Bank();
        Client c = new Client("Paul", "Olar", "Republicii 4", "1920110124601", "paul.olar", "qwerty");
        ClientProc clientProc = new ClientProc();
        //clientProc.restoreClients();
        clientProc.addClient(c);

        Account account = new SavingAccount(new Random().nextInt(10000), c, 1000, 1, new Date());
        Account account2 = new SavingAccount(new Random().nextInt(10000), c, 2000, 2, new Date());
        Account account3 = new SavingAccount(new Random().nextInt(10000), c, 3000, 3, new Date());
        Account account4 = new SavingAccount(new Random().nextInt(10000), c, 4000, 4, new Date());
        Account account5 = new SavingAccount(new Random().nextInt(10000), c, 5000, 5, new Date());
        b.addAccount(c, account);
        b.addAccount(c, account2);
        b.addAccount(c, account3);
        b.addAccount(c, account4);
        b.addAccount(c, account5);
        
        // Apelam metoda de remove client
        b.removeClient(c);
        // Lista ce contine conturile clientului sters ar trebui sa fie null
        assertNull(b.getAccounts(c));
    }
    /**
     * Testam functionalitatea de stergere a unui cont
     */
    @Test
    public void testRemoveAccount() {
        // Creem obiectul de banca si client
        Bank b = new Bank();
        Client c = new Client("Paul", "Olar", "Republicii 4", "1920110124601", "adrian.olar", "qwerty");

        // Adaugam cateva conturi
        Account account = new SavingAccount(new Random().nextInt(10000), c, 1000, 1, new Date());
        Account account2 = new SavingAccount(new Random().nextInt(10000), c, 2000, 2, new Date());
        Account account3 = new SavingAccount(new Random().nextInt(10000), c, 3000, 3, new Date());
        Account account4 = new SavingAccount(new Random().nextInt(10000), c, 4000, 4, new Date());
        Account account5 = new SavingAccount(new Random().nextInt(10000), c, 5000, 5, new Date());

        b.addAccount(c, account);
        b.addAccount(c, account2);
        // Verificam size-ul listei de conturi, deja ar trebui sa fie 2
        assertEquals(2, b.getAccounts(c).size());
        b.addAccount(c, account3);
        // Verificam din nou, ar trebui sa fie 3
        assertEquals(3, b.getAccounts(c).size());
        // Verificam size-ul hash map-ului de conturi, ar trebui sa contina o singura cheie deoarece avem un singur client
        assertEquals(1, b.getAccounts().size());
        // Acest client are 3 conturi
        assertEquals(3, ((List<Account>) b.getAccounts().get(c)).size());

        // Mai adaugam cateva conturi
        b.addAccount(c, account4);
        b.addAccount(c, account5);
        assertEquals(5, b.getAccounts(c).size());

        // Apelam metoda de remove account
        b.removeAccount(c, account);
        // Verifica size-ul dupa stergere, ar trebui sa fie 4
        assertEquals(4, b.getAccounts(c).size());
    }
    
    /**
     * Testam functionalitatea de adaugare cont
     */
    @Test
    public void testBankAddAccount() {
        // Creem banca, un client si adaugam un cont
        Bank b = new Bank();
        Client c = new Client("Paul", "Olar", "Republicii 4", "1920110124601", "adrian.olar", "qwerty");
        Account account = new SavingAccount(new Random().nextInt(10000), c, 1000, 1, new Date());
        b.addAccount(c, account);

        // Verificam size-ul listei de conturi
        assertEquals(1, b.getAccounts().size());
        List<Account> clientAccounts = b.getAccounts(c);
        assertEquals(1, clientAccounts.size());
        // Verificam ca lista proaspat creata contine contul adaugat
        assertTrue(clientAccounts.contains(account));
    }
    
	
    @Test
public void testBankRestoreAndUpdate() {
    Bank b = new Bank();
    // Restauram obiectul de banca din fisierul corect
    b = b.restoreAccounts("accounts.txt");
    // Verificam ca avem ceva date in fisier
    assertTrue(b.getAccounts().size() > 0);
}
    /**
     * Testam cazul in care nu putem restaura obiectul de banca pentru ca numele fisierului in care se salveaza datele este gresit
     */
    @Test
    public void testRestoreAccountsFailure() {
        Bank b = new Bank();
        // Apelam metoda de restore accounts cu un nume de fisier incorect
        b = b.restoreAccounts("some-weird-name.mp4");
        // Obiectul de bank ar trebui sa fie null
        assertNull(b);

    }
}
