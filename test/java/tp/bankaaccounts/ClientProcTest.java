package tp.bankaaccounts;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static junit.framework.TestCase.*;

/**
 * Clasa care testeaza functionalitatea clasei ClientProc
 */
@RunWith(JUnit4.class)
public class ClientProcTest {

    /**
     * Testam constructorul clasei
     */
    @Test
    public void testClientProcCreate() {
        ClientProc clientProc = new ClientProc();
        // Constructorul ar trebui sa initializeze lista de clienti, deci sa nu fie null
        assertNotNull(clientProc.getClients());
        // Size-ul listei de clienti ar trebui sa fie 0.
        assertEquals(0, clientProc.getClients().size());
    }

    /**
     * Testam metoda de adaugare client
     */
    @Test
    public void testAddClient() {
        ClientProc clientProc = new ClientProc();
        Client c = new Client("Paul", "Olar", "Republicii 4", "1920110124601", "adrian.olar", "qwerty");

        // Adaugam clientul
        clientProc.addClient(c);
        // Sizeul listei de clienti ar trebui sa fie 1
        assertEquals(1, clientProc.getClients().size());

    }

    /**
     * Testam metoda de cautare client
     */
    @Test
    public void testSearchClient() {
        ClientProc clientProc = new ClientProc();
        Client c = new Client("Paul", "Olar", "Republicii 4", "1920110124601", "adrian.olar", "qwerty");

        // Adaugam clientul
        clientProc.addClient(c);
        assertNotNull(clientProc.searchClient(c));
        // Verificam informatiile despre clientul cautat sunt corecte
        Client searchedClient = clientProc.searchClient(c);
        assertEquals("Olar", searchedClient.getFirstName());
        assertEquals("Paul", searchedClient.getLastName());
    }

    /**
     * Testam metoda de cautare client dupa CNP
     */
    @Test
    public void testSearchClientByCNP() {
        ClientProc clientProc = new ClientProc();
        Client c = new Client("Paul", "Olar", "Republicii 4", "1920110124601", "adrian.olar", "qwerty");

        // Adaugam clientul
        clientProc.addClient(c);
        assertNotNull(clientProc.searchByCnp("1920110124601"));
        // Verificam informatiile despre clientul cautat sunt corecte
        Client searchedClient = clientProc.searchByCnp("1920110124601");
        assertEquals("Olar", searchedClient.getFirstName());
        assertEquals("Paul", searchedClient.getLastName());
    }

    /**
     * Testam metoda de stergere client
     */
    @Test
    public void testDeleteClient() {
        ClientProc clientProc = new ClientProc();
        Client c = new Client("Paul", "Olar", "Republicii 4", "1920110124601", "adrian.olar", "qwerty");

        // Adaugam clientul
        clientProc.addClient(c);
        // Sizeul listei de clienti ar trebui sa fie 1
        assertEquals(1, clientProc.getClients().size());
        // Stergem clientul
        clientProc.deleteClient(c);
        // Sizeul listei de clienti ar trebui sa fie 0
        assertEquals(0, clientProc.getClients().size());
    }

    /**
     * Testeaza metoda de restaurare clienti din fisier
     */
    @Test
    public void testRestoreClient() {
        ClientProc clientProc = new ClientProc();
        clientProc = clientProc.restoreClients();

        // Size-ul listei de clienti ar trebui sa fie mai mare decat 0
        assertTrue(clientProc.getClients().size() > 0);
    }

}
